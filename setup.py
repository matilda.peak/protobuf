#!/usr/bin/env python3

# Setup module for the Protocol Buffer project
#
# July 2018

from setuptools import setup
import os

# Pull in the essential run-time requirements
# We use an external file so we can do a
# `pip install -r runtime-build-requirements.txt`
# when building a Docker image.
with open('requirements.txt') as file:
    requirements = file.read().splitlines()


# Use the README.rst as the long description.
def get_long_description():
    return open('README.rst').read()


setup(

    name='matildapeak-protobuf',
    version=os.environ.get('CI_COMMIT_TAG', '2018.1'),
    author='Alan Christie',
    author_email='alan.christie@matildapeak.com',
    url='https://gitlab.com/matilda.peak/protobuf',
    license='MIT',
    description='Cross-product protocol buffers',
    long_description=get_long_description(),
    keywords='protobuf protoc messaging',
    platforms=['any'],

    # Our modules to package
    package_dir={'': 'src/main/proto'},
    packages=['matildapeak.chronicler'],

    # Project classification:
    # https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Other Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Operating System :: POSIX :: Linux',
    ],

    install_requires=requirements,

    zip_safe=False,

)
