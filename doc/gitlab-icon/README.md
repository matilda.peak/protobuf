# GitLab Icon
The icon is from [pixabay] and is released as CC0 Creative Commons
which is free for commercial use and no attribution is required.

---

[pxabay]: https://pixabay.com/en/message-icon-material-icon-message-3357821/
