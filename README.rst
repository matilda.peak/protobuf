Cross-product protocol buffers
==============================

.. image:: https://gitlab.com/matilda.peak/protobuf/badges/master/pipeline.svg
   :target: https://gitlab.com/matilda.peak/protobuf
   :alt: Pipeline Status (protobuf)

.. image:: https://badge.fury.io/py/matildapeak-protobuf.svg
   :target: https://badge.fury.io/py/matildapeak-protobuf
   :alt: PyPI package (latest)

.. image:: https://api.bintray.com/packages/matildapeak/protobuf/matildapeak-protobuf/images/download.svg
   :target: https://bintray.com/matildapeak/protobuf/matildapeak-protobuf/_latestVersion
   :alt: Jcenter download (latest)

A library of python bindings for protocol buffer definitions used by one or
more products in the Matilda Peak product suite.

The protocol buffers are used across multiple components and languages.
At the outset we anticipate supporting Python, and Java/Scala. The root
of all packages is ``src/main`` as required by build tools like ``Gradle``.
From there the directory must be ``matildapeak`` followed by component
directories or a ``common`` directory. An example protocol message
file might be::

    src/main/proto/matildapeak/patterncomponent/DelayMessage.proto

Installation
============

The protocol buffers are published on `PyPI`_ and can be installed from
there::

    pip install matildapeak-protobuf

.. _PyPI: https://pypi.org/project/matildapeak-protobuf

Get in touch
============

- Report bugs, suggest features or view the source code `on GitLab`_.

.. _on GitLab: https://gitlab.com/matilda.peak/protobuf

Fixing the Python import
------------------------
The Python protocol buffer bindings use *relative* imports and do not
function properly (at least this is true of Python 3.6 and
protobuf 3.1.0).

The issue appears to be well `documented`_ but a workaround is to adjust
the system path to include the directory of the component's
site package. In a clean virtual environment this is easily achieved
with the following::

    import os
    import sys
    sys.path.append(os.path.join(sys.path[-1], 'patterncomponent'))

Rather than use ``[-1]`` a *safer* approach might be to locate the
``site-packages`` directory in the system path and use that - it
may not be the last entry on the path.

.. _documented: https://github.com/google/protobuf/issues/1491
