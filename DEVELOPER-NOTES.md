# Developer notes
Build and deploy instructions are essentially covered in the `.gitlab-ci` file.
 
## GitLab variables
The following variables need to be defined in order to successfully build
and deploy the Python and Java libraries: -

-   TWINE_USERNAME
-   TWINE_PASSWORD
-   BINTRAY_USER
-   BINTRAY_KEY
